# README #

## What is this repository for? ##

* KJD challenge for Marcel Smuts.
* A e-commerce client front-end with CMS management section.
* Built using Vue.js + Node.js + MySql

## Dependencies ##

* MySql
* Node
* NPM
* Vue-CLI

## How do I get set up? ##

* The following setup is for Windows OS:
### MySql ###
* Install MySql: https://dev.mysql.com/downloads/installer/
* If you do not have mysql.exe in your path variables, you can follow the insctructions here to add it to your path variable: https://dev.mysql.com/doc/mysql-windows-excerpt/5.7/en/mysql-installation-windows-path.html
* Open terminal and navigate to the scripts folder inside the project
* Run `mysql -u root -p` to enter the mysql command line interface as the root user. You will be prompted for a password. If you set a password for the root user, enter it now, otherwise the default is no password.
* Run `source ./kjd-db.sql` to execute the database create script found in scripts folder.
* Open `kjd-challenge/server/db.config.js` and set the mysql `USER` and `PASSWORD` fields to the username and password you use to connect to MySql. If this is the first time setting up MySql, you will use 'root' and the password you set during installation.

### Node ###
* Download and install the latest version of node here: https://nodejs.org/en/download/
* Be sure to also install node package manager, included in the node installer.

### Vue-CLI ###
* Open terminal and run `npm install -g @vue/cli`

### Packages
* Open terminal and navigate to the base folder for the project. Run `npm install`

## How to start web application ##

* Open terminal and input "npm start"
* Open your browser of choice and navigate to http://localhost:8081
* The MySql database is set up with a few default users
    * username: admin, password: admin
    * username: publisher, password: publisher
    * username: author, password: author
    * username: client, password: client
* Use one of the above logins to begin