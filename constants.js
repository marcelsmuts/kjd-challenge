export default {
    httpStatusCodes: {
        Success: 200,
        Unauthorized: 401
    }
}