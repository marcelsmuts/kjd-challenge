export default {
    roles: {
        admin: {
            id: 1,
            name: "Admin"
        },
        publisher: {
            id: 2,
            name: "Publisher"
        },
        author: {
            id: 3,
            name: "Author"
        },
        client: {
            id: 4,
            name: "Client"
        }
    },
    getRolesArray() {
        return [
            this.roles.admin,
            this.roles.publisher,
            this.roles.author,
            this.roles.client
        ]
    },
    storageKeys: {
        roleId: 'role-id',
        userId: 'user-id',
        cart: 'cart'
    }
}