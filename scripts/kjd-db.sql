-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.23 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for kjd-db
CREATE DATABASE IF NOT EXISTS `kjd-db` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `kjd-db`;

-- Dumping structure for table kjd-db.lookup_roles
CREATE TABLE IF NOT EXISTS `lookup_roles` (
  `id` int NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table kjd-db.lookup_roles: ~4 rows (approximately)
/*!40000 ALTER TABLE `lookup_roles` DISABLE KEYS */;
INSERT INTO `lookup_roles` (`id`, `name`) VALUES
	(1, 'Admin'),
	(2, 'Publisher'),
	(3, 'Author'),
	(4, 'Client');
/*!40000 ALTER TABLE `lookup_roles` ENABLE KEYS */;

-- Dumping structure for table kjd-db.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(500) NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `published_date` timestamp NULL DEFAULT NULL,
  `published_by` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_products_users` (`published_by`),
  CONSTRAINT `FK_products_users` FOREIGN KEY (`published_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table kjd-db.products: ~0 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `name`, `description`, `price`, `published_date`, `published_by`) VALUES
	(1, 'Companion Cube', 'Silent Helper. Please do not incinerate.', 10.00, '2021-02-28 20:39:17', 2),
	(2, 'Pip-Boy', 'Wrist-based computer. Available in various colours.', 350.50, '2021-02-28 20:39:17', 2),
	(3, 'Long Straight Piece', 'Sweet saviour!', 5.00, '2021-02-28 20:39:17', 2),
	(4, 'Crowbar', 'Headcrabs be gone.', 3.00, '2021-02-28 20:39:18', 2),
	(5, 'Cardboard box', 'They will never see you.', 0.60, '2021-02-28 18:08:58', 2);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Dumping structure for table kjd-db.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `role_id` int NOT NULL,
  `username` varchar(50) NOT NULL,
  `first_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `last_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `password_hash` varchar(200) NOT NULL,
  `password_salt` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__lookup_roles` (`role_id`),
  CONSTRAINT `FK__lookup_roles` FOREIGN KEY (`role_id`) REFERENCES `lookup_roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table kjd-db.users: ~4 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `role_id`, `username`, `first_name`, `last_name`, `password_hash`, `password_salt`) VALUES
	(1, 1, 'admin', 'Geralt', 'of Rivia', '1614380889448$10$8305f54cc837fda9d653d6fa4e933ffe', '1614380889448'),
	(2, 2, 'publisher', 'Master', 'Chief', '1614380984856$10$1fd198e535647bc99d325859aba0a9cf', '1614380984856'),
	(3, 3, 'author', 'Solid', 'Snake', '1614381020389$10$da048ff08aa39c6b82c2c63b4c506528', '1614381020389'),
	(4, 4, 'client', 'Samus', 'Aran', '1614381051604$10$4a7590a5dfe67a0a96a8745326d694de', '1614381051604');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
