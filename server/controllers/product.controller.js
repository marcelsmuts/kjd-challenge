import db from '../models/index.js';
import fs from 'fs';

export default {
    getAll: async (req, res) => {
        const products = await db.sequelize.query(`
            select *
            from products;`, {
            model: db.Product,
            mapToModel: true
        });

        res.send(products);
    },
    getPublishedProducts: async (req, res) => {
        const products = await db.sequelize.query(`
            select *
            from products
            where published_date is not null;`, {
            model: db.Product,
            mapToModel: true
        });

        res.send(products);
    },
    getById: async (req, res) => {
        const id = parseInt(req.query.id);
        const products = await db.sequelize.query(`
        select *
        from products
        where id = :id
        limit 1`, {
            replacements: { id },
            model: db.Product,
            mapToModel: true
        });

        if (!products.length) {
            return res.status(404).send({
                message: "Could not find product."
            });
        }

        res.send(products[0]);
    },
    save: async (req, res) => {
        const product = req.body.product;
        const imgData = product.imageData.replace(/^data:image\/\w+;base64,/, "");
        var buf = Buffer.from(imgData, 'base64');
        fs.writeFile(`./src/assets/${product.name}.png`, buf, 'binary', async function(err){
            if (err) throw err
            console.log('File saved.')
            const replacements = {
                id: product.id,
                name: product.name,
                description: product.description,
                price: product.price
            }
            if (product.id > 0) {
                await db.sequelize.query(`
                update products
                set
                    name = :name,
                    description = :description,
                    price = :price
                where id = :id`, {
                    replacements
                });
    
                res.status(200).send();
            } else {
                await db.sequelize.query(`
                insert into products
                (
                    name,
                    description,
                    price
                )
                values
                (
                    :name,
                    :description,
                    :price
                );`, {
                    replacements
                });
            }
            
             res.status(200).send();
        })
    },
    delete: async (req, res) => {
        const id = parseInt(req.query.id);
        await db.sequelize.query(`
            delete from products
            where id = :id`, {
                replacements: {
                    id
                }
            });

        res.status(200).send();
    },
    publish: async (req, res) => {
        const productId = req.body.productId;
        const userId = req.body.userId;
        await db.sequelize.query(`
            update products
            set
                published_date = utc_timestamp(),
                published_by = :userId
            where id = :productId`, {
                replacements: {
                    productId,
                    userId
                }
            });

        res.status(200).send();
    },
    unpublish: async (req, res) => {
        const id = req.query.id;
        await db.sequelize.query(`
            update products
            set
                published_date = NULL
            where id = :id`, {
                replacements: {
                    id
                }
            });

        res.status(200).send();
    }
}
