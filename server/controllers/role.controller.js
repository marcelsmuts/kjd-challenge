import db from '../models/index.js';
const Role = db.roles;

export default {
    getById: (req, res) => {
        const id = req.params.id;
    
        Role.findByPk(id)
            .then(data => {
                res.send(data);
            })
            .catch(err => {
                res.status(500).send({
                    message: "Error retrieving Role with id=" + id
                }, err);
            });
    }
}