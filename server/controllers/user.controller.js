import hasher from '../helpers/hasher.js';
import db from '../models/index.js';
import { v4 as uuidv4 } from 'uuid';
import cookie from 'cookie';
import appSettings from '../../app-settings.js';

export default {
    signIn: async (req, res) => {
        const username = req.body.username;
        const password = req.body.password;
        const rememberMe = req.body.rememberMe;

        const users = await db.sequelize.query(`
            select * from users u 
            inner join lookup_roles r
            on u.role_id = r.id
            where username = :username
            limit 1`, {
            replacements: { username },
            model: db.User,
            mapToModel: true
        });

        if (!users.length) {
            return errorSigningIn(res);
        }

        const user = users[0];
        const hashedPass = hasher.hash(
            password,
            {
                salt: user.passwordSalt
            });
        if (user.passwordHash !== hashedPass) {
            return errorSigningIn(res);
        }

        if (rememberMe) {
            const sessionToken = uuidv4();
            // Usually I would store the session token in the database but I'm running this site on localhost for now.
            // Cookies won't work on localhost.
            res.setHeader('Set-Cookie', cookie.serialize('session', sessionToken, {
                maxAge: appSettings.cookieExpiryLength,
                domain: null
            }));
        }

        res.send({
            roleId: user.roleId,
            userId: user.id
        });
    },
    getAll: async (req, res) => {
        const users = await db.sequelize.query(`
            select 
                id,
                first_name,
                last_name,
                role_id
            from users u`, {
            model: db.User,
            mapToModel: true
        });

        res.send(users);
    },
    getById: async (req, res) => {
        const id = parseInt(req.query.id);
        const users = await db.sequelize.query(`
        select
            id,
            username,
            first_name,
            last_name,
            role_id
        from users u
        where id = :id
        limit 1`, {
            replacements: { id },
            model: db.User,
            mapToModel: true
        });

        if (!users.length) {
            return res.status(404).send({
                message: "Could not find user."
            });
        }

        res.send(users[0]);
    },
    saveUser: async (req, res) => {
        const user = req.body.user;
        const replacements = {
            id: user.id,
            username: user.username,
            firstName: user.firstName,
            lastName: user.lastName,
            roleId: user.roleId
        }
        if (user.id > 0) {
            await db.sequelize.query(`
            update users
            set
                username = :username,
                first_name = :firstName,
                last_name = :lastName,
                role_id = :roleId
            where id = :id`, {
                replacements
            });
        } else {
            const salt = new Date().getTime();
            replacements.passwordHash = hasher.hash(user.password, salt);
            replacements.passwordSalt = salt;
            await db.sequelize.query(`
            insert into users
            (
                username,
                first_name,
                last_name,
                role_id,
                password_hash,
                password_salt
            )
            values
            (
                :username,
                :firstName,
                :lastName,
                :roleId,
                :passwordHash,
                :passwordSalt
            );`, {
                replacements
            });
        }

        res.status(200).send();
    },
    deleteUser: async (req, res) => {
        const id = parseInt(req.query.id);
        await db.sequelize.query(`
            delete from users
            where id = :id`, {
                replacements: {
                    id
                }
            });

        res.status(200).send();
    }
}

const errorSigningIn = (res) => {
    return res.status(401).send({
        message: "Could not sign user in."
    });
}
