import md5 from 'md5';

export default {
    hash(raw, options = {}) {
        const salt = options.salt ? options.salt : new Date().getTime();
        const rounds = options.rounds ? options.rounds : 10;

        let hashed = md5(raw + salt);
        for (let i = 0; i <= rounds; i++) {
            hashed = md5(hashed);
        }
        return `${salt}$${rounds}$${hashed}`;
    },
    compare(raw, hashed) {
        try {
          const [ salt, rounds ] = hashed.split('$');
          const hashedRawValue = this.hash(raw, { salt, rounds });
          return hashed === hashedRawValue;
        } catch (error) {
          throw Error(error.message);
        }
      }
}




