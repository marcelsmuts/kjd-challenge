import dbConfig from '../db.config.js';
import Sequelize from 'sequelize';

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

import roleModel from './role.model.js';
db.Role = roleModel(sequelize, Sequelize);
import userModel from './user.model.js';
db.User = userModel(sequelize, Sequelize);
import productModel from './product.model.js';
db.Product = productModel(sequelize, Sequelize);

export default db;