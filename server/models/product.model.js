export default (sequelize, Sequelize) => {
    const Product = sequelize.define("product", {
      name: {
        type: Sequelize.STRING(100)
      },
      description: {
        type: Sequelize.STRING(500)
      },
      price: {
          type: Sequelize.DECIMAL(10, 2) // This should probably be an integer if we want to convert to other currencies
      },
      publishedDate: {
        type: Sequelize.DATE
      },
      publishedBy: {
        type: Sequelize.INTEGER
      }
    }, {
      timestamps: false,
      underscored: true
    });
  
    return Product;
  };
  