export default (sequelize, Sequelize) => {
  const Role = sequelize.define("lookup_roles", {
    name: {
      type: Sequelize.STRING(50),
      allowNull: false
    }
  }, {
    timestamps: false,
    underscored: true
  });

  return Role;
};
