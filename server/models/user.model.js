export default (sequelize, Sequelize) => {
  const User = sequelize.define("user", {
    username: {
      type: Sequelize.STRING(100)
    },
    roleId: {
      type: Sequelize.INTEGER
    },
    passwordHash: {
      type: Sequelize.STRING(200)
    },
    passwordSalt: {
      type: Sequelize.STRING(200)
    },
    firstName: {
      type: Sequelize.STRING(50)
    },
    lastName: {
      type: Sequelize.STRING(50)
    }
  }, {
    timestamps: false,
    underscored: true
  });

  return User;
};
