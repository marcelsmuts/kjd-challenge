import productController from '../controllers/product.controller.js';
import express from 'express';

export default app => {
  
    var router = express.Router();
  
    router.get("/all", productController.getAll);
    router.get("/published", productController.getPublishedProducts);
    router.get("/", productController.getById);
    router.put("/", productController.save);
    router.delete("/", productController.delete);
    router.put("/publish", productController.publish);
    router.put("/unpublish", productController.unpublish);

    app.use('/api/product', router);
}