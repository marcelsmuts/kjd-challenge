import roleController from '../controllers/role.controller.js';
import express from 'express'

export default app => {
    
    var router = express.Router();
  
    router.get("/:id", roleController.getById);

    app.use('/api/roles', router);
}