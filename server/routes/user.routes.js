import userController from '../controllers/user.controller.js';
import express from 'express';

export default app => {
  
    var router = express.Router();
  
    router.post("/signin", userController.signIn);
    router.get("/all", userController.getAll);
    router.get("/", userController.getById);
    router.put("/", userController.saveUser);
    router.delete("/", userController.deleteUser);

    app.use('/api/user', router);
}