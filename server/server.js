import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import db from './models/index.js';

const app = express();

var corsOptions = {
  origin: 'http://localhost:8081'
};

app.use(cors(corsOptions));
app.use(bodyParser.json({ limit: '50mb', extended: true }));
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));

var router = express.Router();

// Middleware
router.use(function(req, res, next) {
  console.log("Incoming request");
  next(); 
});

app.use('/api', router);

// ROUTES
import roleRoutes from './routes/role.routes.js';
roleRoutes(app);
import userRoutes from './routes/user.routes.js';
userRoutes(app);
import productRoutes from './routes/product.routes.js';
productRoutes(app);

db.sequelize.sync();

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

export default app;