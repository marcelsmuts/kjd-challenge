import apiService from '../../services/api-service.js';
import navigationService from '../../services/navigation-service.js';
import permissionService from '../../services/permission-service';
import storageService from '../../services/storage-service.js';
import lookups from "../../../lookups";
import Vue from 'vue';
import { required } from 'vuelidate/lib/validators';

export default {
  name: 'EditProduct',
  validations() {
    return {
      product: {
        name: {
          required
        },
        description: {
          required
        },
        price: {
          required
        }
      }
    }
  },
  data() {
    return {
      product: {
        image: "",
        price: 0.00
      },
      currentUserId: 0,
      requiresImage: false
    };
  },
  methods: {
    isEdit() {
      return this.product
        && this.product.id;
    },
    restrictDecimal() {
      this.product.price = this.product.price.match(/^\d+\.?\d{0,2}/);
    },
    async save() {
      this.requiresImage = false;
      this.$v.$touch();
      if (this.$v.$invalid) {
        return;
      }

      if(!this.product.imageData) {
        this.requiresImage = true;
        return;
      }
      
      await apiService.Products.save(this.product);
      Vue.$toast.success('Saved successfully!');

      navigationService.navigateToManagement();
    },
    async publish() {
      await apiService.Products.publish(
        this.product.id,
        this.currentUserId);
      this.product.publishedDate = new Date();
      Vue.$toast.success('Published successfully!');
    },
    async unpublish() {
      await apiService.Products.unpublish(
        this.product.id);
      this.product.publishedDate = undefined;
      Vue.$toast.success('Unpublished successfully!');
    },
    imageChanged(name, files) {
      if (!files.length) {
        this.product.imageData = "";
        return;
      }

      const imageFile = files[0];

      var match = ["image/jpeg", "image/png", "image/jpg"];
      if (!((imageFile.type == match[0])
        || (imageFile.type == match[1])
        || (imageFile.type == match[2]))) {
        Vue.$toast.error('Please upload a valid image.');
        return;
      }

      const onLoadCallback = function (e) {
        this.product.imageData = e.target.result;
        this.$forceUpdate();
      }
      const boundCallback = onLoadCallback.bind(this);
  
      var reader = new FileReader();
      reader.onload = boundCallback;
      reader.readAsDataURL(imageFile);
    },
    getImage(name) {
      try {
        return require(`../../assets/${name}.png`);
      } catch {
        // Ignore errors
      }
    },
  },
  async mounted() {
    this.currentUserId = parseInt(storageService.get(lookups.storageKeys.userId));
    const idParam = parseInt(this.$route.params.id);
    if (idParam > 0) {
      if (!permissionService.canEditProducts()) {
        return navigationService.navigateToLogin();
      }

      this.product = await apiService.Products.getById(idParam);
      var image = new Image();
      image.src = this.getImage(this.product.name);
      this.product.imageData = getBase64Image(image);
    } else {
      if (!permissionService.canCreateProducts()) {
        return navigationService.navigateToLogin();
      }
    }
  },
  watch: {
    $route(to, from) {
      if (to !== from) {
        location.reload();
      }
    }
  }
}

function getBase64Image(img) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  return canvas.toDataURL("image/png");
}
