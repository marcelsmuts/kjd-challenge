import lookups from '../../../lookups.js';
import apiService from '../../services/api-service.js';
import navigationService from '../../services/navigation-service.js';
import permissionService from '../../services/permission-service';
import Vue from 'vue';
import { required, sameAs } from 'vuelidate/lib/validators'


export default {
    name: 'EditUser',
    validations () {
      const editRequiredFields = {
        user: {
          username: {
            required
          },
          firstName: {
            required
          },
          lastName: {
            required
          },
          roleId: {
            required
          }
        }
      }
      const createRequiredFields = JSON.parse(JSON.stringify(editRequiredFields));
      createRequiredFields.user.password = {
        required
      };
      createRequiredFields.repeatPassword = {
        sameAsPassword: sameAs(function() { return this.user.password })
      }
      if (this.isEdit()) {
        return editRequiredFields;
      } else {
        return createRequiredFields;
      }
    },
    data() {
        return {
            user: {},
            roles: lookups.getRolesArray(),
            repeatPassword: ""
        };
    },
    methods: {
        isEdit() {
            return this.user
            && this.user.id;
        },
        async saveUser() {
            this.$v.$touch();
            if (this.$v.$invalid) {
                return;
            }

            await apiService.User.saveUser(this.user);
            Vue.$toast.success('Saved successfully!');
            navigationService.navigateToManagement();
        }
    },
    async mounted() {
        if(!permissionService.canManageUsers()) {
            return navigationService.navigateToLogin();
        }

        const idParam = parseInt(this.$route.params.id);
        if(idParam > 0) {
            this.user = await apiService.User.getById(idParam);
        }
    }
}