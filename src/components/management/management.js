import navigationService from '../../services/navigation-service.js';
import ProductManagement from './product-management.vue';
import UserManagement from './user-management.vue';
import permissionService from '../../services/permission-service';

export default {
    name: 'Management',
    components: {
        ProductManagement,
        UserManagement
    },
    data() {
        return {
        };
    },
    methods: {
        canViewUsersPage() {
            return permissionService.canManageUsers();
        },
        canViewProductsPage() {
            return permissionService.canViewProductManagementPage();
        }
    },
    mounted() {
        if(!this.canViewUsersPage()
        && !this.canViewProductsPage()) {
            return navigationService.navigateToLogin();
        }
    }
}