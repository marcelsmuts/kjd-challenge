import lookups from "../../../lookups";
import apiService from "../../services/api-service";
import navigationService from "../../services/navigation-service";
import Vue from 'vue';
import storageService from "../../services/storage-service";
import permissionService from '../../services/permission-service';

export default {
    name: 'ProductManagement',
    data() {
        return {
            products: [],
            currentUserId: 0
        };
    },
    methods: {
        async publishProduct(id) {
            await apiService.Products.publish(
                id,
                this.currentUserId);
            const product = this.products.find((p) => {
                return p.id === id;
            });
            product.publishedDate = new Date();
            Vue.$toast.success('Published successfully!');
        },
        async unpublishProduct(id) {
            await apiService.Products.unpublish(
                id);
            const product = this.products.find((p) => {
                return p.id === id;
            });
            product.publishedDate = undefined;
            Vue.$toast.success('Unpublished successfully!');
        },
        editProduct(productId) {
            navigationService.navigateToEditProduct(productId);
        },
        async deleteProduct(
            productId,
            productName) {
            const result = confirm(`Are you sure you want to delete ${productName}`)
            if (result) {
                await apiService.Products.delete(productId);
                this.products = this.products.filter(function (prod) {
                    return prod.id !== productId
                })
                Vue.$toast.success('Deleted successfully!');
            }
        },
        getImage(name) {
            try {
                return require(`../../assets/${name}.png`);
            } catch {
                // Ignore errors
            }
        },
        canPublish() {
            return permissionService.canPublishProducts();
        },
        canManageProduct() {
            return permissionService.canEditProducts();
        },
        canCreateProducts() {
            return permissionService.canCreateProducts();
        }
    },
    async mounted() {
        if (!permissionService.canViewProductManagementPage()) {
            return navigationService.navigateToLogin();
        }

        this.products = await apiService.Products.getAll();
        this.currentUserId = parseInt(storageService.get(lookups.storageKeys.userId));
    }
}