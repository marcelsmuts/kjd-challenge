import lookups from "../../../lookups";
import apiService from "../../services/api-service";
import navigationService from "../../services/navigation-service";
import Vue from 'vue';
import storageService from "../../services/storage-service";
import permissionService from '../../services/permission-service';

export default {
    name: 'UserManagement',
    data() {
        return {
            users: [],
            currentUserId: 0
        };
    },
    methods: {
        getRoleName(roleId) {
            return lookups.getRolesArray()
                .find((role) => role.id === roleId).name;
        },
        editUser(userId) {
            navigationService.navigateToEditUser(userId);
        },
        async deleteUser(
            id,
            firstname,
            lastname) {
            if(id === this.currentUserId) {
                return;
            }

            const result = confirm(`Are you sure you want to delete ${firstname} ${lastname}`)
            if (result) {
                await apiService.User.deleteUser(id);
                this.users = this.users.filter(function (user) {
                    return user.id !== id
                })
                Vue.$toast.success('Deleted successfully!');
            }
        }
    },
    async mounted() {
        if(!permissionService.canManageUsers()) {
            return navigationService.navigateToLogin();
        }

        this.users = await apiService.User.getAllUsers();
        this.currentUserId = parseInt(storageService.get(lookups.storageKeys.userId));
    }
}