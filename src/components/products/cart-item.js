export default {
    name: 'CartItem',
    props: [
        'product',
        'index',
        'remove']
}