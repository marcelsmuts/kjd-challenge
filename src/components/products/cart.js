import storageService from '../../services/storage-service.js';
import lookups from '../../../lookups.js';
import CartItem from './cart-item.vue';

export default {
    name: 'Cart',
    props: [
        'itemRemoved'
    ],
    components: {
        CartItem
    },
    data() {
        return {
            products: []
        };
    },
    methods: {
        removeItem(index) {
            this.products.splice(index, 1);
            storageService.set(lookups.storageKeys.cart, this.products);
            this.itemRemoved();
        },
        total() {
            return this.products.reduce((accumulator, current) => accumulator + parseFloat(current.price), 0).toFixed(2);
        }
    },
    async mounted() {
        const cartFromStorage = JSON.parse(storageService.get(lookups.storageKeys.cart));
        this.products = cartFromStorage ? cartFromStorage : this.products;
    }
}