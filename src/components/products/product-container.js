export default {
    name: 'Products',
    data() {
        return {
        };
    },
    props: [
        'product',
        'addToCart'],
    methods: {
        getImage(name) {
            try {
                return require(`../../assets/${name}.png`);
            } catch {
                // Ignore errors
            }
        },
    },
    async mounted() {
    }
}