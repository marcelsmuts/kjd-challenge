import permissionService from '../../services/permission-service.js';
import navigationService from '../../services/navigation-service.js';
import apiService from '../../services/api-service.js';
import storageService from '../../services/storage-service.js';
import lookups from "../../../lookups";
import ProductContainer from './product-container.vue';
import Cart from './cart.vue';

export default {
    name: 'Products',
    components: {
        ProductContainer,
        Cart
    },
    data() {
        return {
            products: [],
            productsInCart: []
        };
    },
    methods: {
        totalPrice() {
            return this.productsInCart.reduce((accumulator, current) => accumulator + parseFloat(current.price), 0).toFixed(2);
        },
        addToCart(product) {
            this.productsInCart.push(product);
            storageService.set(lookups.storageKeys.cart, this.productsInCart);
        },
        itemRemoved() {
            this.productsInCart = JSON.parse(storageService.get(lookups.storageKeys.cart));
        },
    },
    async mounted() {
        if (!permissionService.canViewClientProductsPage()) {
            return navigationService.navigateToLogin();
        }

        this.products = await apiService.Products.getPublishedProducts();
        this.currentUserId = parseInt(storageService.get(lookups.storageKeys.userId));
        const cartFromStorage = JSON.parse(storageService.get(lookups.storageKeys.cart));
        this.productsInCart = cartFromStorage ? cartFromStorage : this.productsInCart;
    }
}