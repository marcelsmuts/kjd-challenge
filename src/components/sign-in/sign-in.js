import lookups from '../../../lookups.js';
import constants from '../../../constants.js';
import navigationService from '../../services/navigation-service.js';
import authenticationService from '../../services/authentication-service.js';
import storageService from '../../services/storage-service.js';
import Vue from 'vue';

export default {
  name: 'SignIn',
  data() {
    return {
      roles: [],
      username: "",
      password: "",
      rememberMe: false,
      loginFailed: false
    };
  },
  methods: {
    async signIn() {
      const result = await authenticationService.authenticateUser(
        this.username,
        this.password,
        this.rememberMe
      )

      if (result.status === constants.httpStatusCodes.Unauthorized) {
        Vue.$toast.error('Login failed');
        return;
      }

      if (result.status === constants.httpStatusCodes.Success) {
        Vue.$toast.clear()
        // We should authenticate every request, but i'm not using cookies, so for the purposes of this challenge I'm just going to store the role in local storage.
        storageService.set(lookups.storageKeys.roleId, result.data.roleId);
        storageService.set(lookups.storageKeys.userId, result.data.userId);
        this.navigateUser(result.data.roleId);
      }
    },
    navigateUser(roleId) {
      switch (roleId) {
        case lookups.roles.admin.id:
        case lookups.roles.publisher.id:
        case lookups.roles.author.id:
          navigationService.navigateToManagement();
          break;
        case lookups.roles.client.id:
          navigationService.navigateToProducts();
          break;
      }
      return roleId;
    }
  },
  mounted() {
    this.$refs.username.focus();
  }
}