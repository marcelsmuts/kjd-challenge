import Vue from 'vue';
import router from './router';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BootstrapVue } from 'bootstrap-vue';
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import Vuelidate from 'vuelidate';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faEdit, faTimes, faCheck, faCartPlus, faShoppingCart, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { faCheckSquare } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

addIcons();
Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.use(BootstrapVue);
Vue.use(VueToast);
Vue.use(Vuelidate);

new Vue({
  router
}).$mount('#app')

function addIcons() {
  library.add(faEdit);
  library.add(faCheckSquare);
  library.add(faTimes);
  library.add(faCheck);
  library.add(faCartPlus);
  library.add(faShoppingCart);
  library.add(faTrashAlt);
}
