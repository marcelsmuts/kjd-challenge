import Vue from 'vue';
import VueRouter from 'vue-router';
import Management from './components/management/management.vue';
import EditUser from './components/management/edit-user.vue';
import EditProduct from './components/management/edit-product.vue';
import SignIn from './components/sign-in/sign-in.vue';
import Products from './components/products/products.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'login',
    component: Vue.component('sign-in', SignIn)
  },
  {
    path: '/management',
    name: 'management',
    component: Vue.component('management', Management),
  },
  {
    path: '/products',
    name: 'products',
    component: Vue.component('products', Products)
  },
  {
    path: '/management/user/:id',
    name: 'edit-user',
    component: Vue.component('edit-user', EditUser)
  },
  {
    path: '/management/product/:id',
    name: 'edit-product',
    component: Vue.component('edit-product', EditProduct)
  }
]

const router = new VueRouter({
  routes
});

export default router;