import http from '../http-common.js';

export default {
    User: {
        signIn: async (
            username,
            password,
            rememberMe) => {
            // We could hash the password before sending it over HTTP but implementing SSL should protect it.
            return await executeRequest(
                http.post, 
                `/user/signin`,
                {
                    username,
                    password,
                    rememberMe
                });
        },
        getAllUsers: async () => {
            return (await executeRequest(
                http.get, 
                `/user/all`)).data;
        },
        getById: async (id) => {
            return (await executeRequest(
                http.get, 
                `/user?id=${id}`)).data;
        },
        saveUser: async (user) => {
            return (await executeRequest(
                http.put, 
                `/user`, { user })).data;
        },
        deleteUser: async (id) => {
            return (await executeRequest(
                http.delete, 
                `/user?id=${id}`)).data;
        }
    },
    Products: {
        getAll: async () => {
            return (await executeRequest(
                http.get, 
                `/product/all`)).data;
        },
        getPublishedProducts: async () => {
            return (await executeRequest(
                http.get, 
                `/product/published`)).data;
        },
        getById: async (id) => {
            return (await executeRequest(
                http.get, 
                `/product?id=${id}`)).data;
        },
        save: async (product) => {
            return (await executeRequest(
                http.put, 
                `/product`,
                { product })).data;
        },
        delete: async (id) => {
            return (await executeRequest(
                http.delete, 
                `/product?id=${id}`)).data;
        },
        publish: async (
            productId,
            userId) => {
            return await executeRequest(
                http.put, 
                `/product/publish`,
                {
                    userId,
                    productId
                }).data;
        },
        unpublish: async (id) => {
            return await executeRequest(
                http.put, 
                `/product/unpublish?id=${id}`).data;
        },
    }
}

const executeRequest = async (method, route, params) => {
    const result = await method(route, params).catch((err) => {
        return err.response;
    });

    return result;
}
