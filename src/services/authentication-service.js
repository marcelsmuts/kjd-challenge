import apiService from './api-service.js';

export default {
  async authenticateUser(
    username, 
    password,
    rememberMe) {
    return apiService.User.signIn(
      username,
      password,
      rememberMe
    );
  }
}