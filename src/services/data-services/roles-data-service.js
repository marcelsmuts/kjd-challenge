import http from '../../http-common.js';

export default {
  getById(id) {
    return http.get(`/roles/${id}`);
  }
}
