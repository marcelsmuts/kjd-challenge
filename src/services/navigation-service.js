import router from '../router.js';

export default {
    navigateToManagement() {
        router.push({ name: 'management' });
    },
    navigateToProducts() {
        router.push({ name: 'products' });
    },
    navigateToLogin() {
        router.push({ name: 'login' });
    },
    navigateToEditUser(userId) {
        router.push({ name: 'edit-user', params: { id: userId } });
    },
    navigateToEditProduct(productId) {
        router.push({ name: 'edit-product', params: { id: productId } });
    }
}