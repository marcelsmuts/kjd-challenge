import lookups from "../../lookups";
import storageService from "./storage-service";

export default {
    canManageUsers() {
        return getRoleId() === lookups.roles.admin.id;
    },
    canEditProducts() {
        const roleId = getRoleId();
        return [
            lookups.roles.publisher.id, 
            lookups.roles.admin.id]
            .includes(roleId);
    },
    canPublishProducts() {
        return getRoleId() === lookups.roles.publisher.id;
    },
    canCreateProducts() {
        return getRoleId() === lookups.roles.author.id;
    },
    canViewClientProductsPage() {
        return getRoleId() === lookups.roles.client.id;
    },
    canViewProductManagementPage() {
        return this.canPublishProducts() 
        || this.canCreateProducts()
        || this.canEditProducts()
    }
}

function getRoleId() {
    return parseInt(storageService.get(lookups.storageKeys.roleId));
}