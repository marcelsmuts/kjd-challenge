export default {
    set(key, item) {
        window.localStorage.setItem(
            key, 
            JSON.stringify(item));
    },
    get(key) {
        return window.localStorage.getItem(key);
    }
}